(defsystem cl-json-object
  :name "cl-json-object"
  :description "Advanced integration between CLOS and JSON based on CL-JSON."
  :author "Alexey Martynov"
  :license "MIT"
  :in-order-to ((test-op (test-op cl-json-object/tests)))
  :depends-on (#:cl-json
               #:alexandria
               #:local-time
               #:closer-mop)
  :components ((:module "src"
                        :components ((:file "packages")
                                     (:file "json-object" :depends-on ("packages"))
                                     (:file "json-renderer" :depends-on ("json-object"))
                                     (:file "type-support")
                                     (:file "json-parser" :depends-on ("json-object" "type-support"))))))

(defsystem cl-json-object/tests
  :name "cl-json-object/tests"
  :description "Advanced integration between CLOS and JSON based on CL-JSON."
  :author "Alexey Martynov"
  :license "MIT"
  :depends-on (#:cl-json-object #:fiveam)
  :components ((:module "test"
                :components ((:file "runner")
                             (:file "type-support" :depends-on ("runner"))
                             (:file "test-classes" :depends-on ("runner"))
                             (:file "json-renderer" :depends-on ("test-classes"))
                             (:file "json-parser" :depends-on ("test-classes"))))))
