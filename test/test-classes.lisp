(in-package :cl-json-object-tests)

(defclass test-reference ()
  ((id :initarg :id :initform (error "ID is required"))
   (description :initarg :description
                :json-type :scalar))
  (:metaclass json-object-class)
  (:keys id)
  )

(defclass test-object ()
  ((id :initarg :id :initform (error "ID is required"))
   (description :initarg :description
                :json-type :scalar)
   (reference :initarg :reference
              :json-property "subObject"
              :json-type (:class test-reference))
   (list-as-object :initarg :list-as-object
                   :json-type :a-list)
   (list-as-object-keyword :initarg :list-as-object-keyword
                   :json-type (:a-list :package :keyword))
   (list-as-object-string :initarg :list-as-object-string
                          :json-type (:a-list :string))
   (ignored :initarg :ignored :json-ignore t)
   (render-ignored :initarg :render-ignored :json-ignore :render)
   (parse-ignored :initarg :parse-ignored :json-ignore :parse)
   (uninitialized-slot))
  (:metaclass json-object-class)
  (:keys id)
  )

(deftype simple-enum ()
  '(member value-1 value-2))

(define-json-enum-converter integer-enum-converter
  (value-1 11)
  (value-2 12))

(defclass test-enums ()
  ((id :initarg :id :initform (error "ID is required"))
   (enum :initarg :enum
         :json-type :enum)
   (package-enum :initarg :package-enum
                 :json-type (:enum :package :cl-json-object-tests.values))
   (deprecated-package-enum :initarg :deprecated-package-enum
                 :json-type (:enum :cl-json-object-tests.values))
   (keyword-enum :initarg :keyword-enum
                 :json-type :keyword)
   (integer-enum :initarg :integer-enum
                 :json-type (:enum :converter integer-enum-converter)))
  (:metaclass json-object-class)
  (:keys id)
  )

(defun string-converter (direction value)
  (declare (ignore value))
  (ecase direction
    (:render
     "CONVERTED")
    (:parse
     "RESTORED")))

(defclass test-conversion ()
  ((id :initarg :id)
   (integer :initarg :integer
            :type (or null integer))
   (float :initarg :float
          :type (or null (float -10.0 10.0)))
   (string :initarg :string
           :type (or null string))
   (timestamp :initarg :timestamp
              :json-type :timestamp)
   (vector :initarg :vector
           :type (or null (vector integer)))
   (string-vector :initarg :string-vector
                  :type (or null (vector string)))
   (enum-vector :initarg :enum-vector
                :json-type (:enum :converter integer-enum-converter)
                :type (or null (vector simple-enum)))
   (converted-string :initarg :converted-string
                     :json-type (:scalar :converter string-converter)))
  (:metaclass json-object-class)
  (:keys id))

(closer-mop:finalize-inheritance (find-class 'test-object))
(closer-mop:finalize-inheritance (find-class 'test-reference))
(closer-mop:finalize-inheritance (find-class 'test-enums))
(closer-mop:finalize-inheritance (find-class 'test-conversion))
