(in-package :cl-json-object-tests)

(def-suite json-renderer :in cl-json-object)

(in-suite json-renderer)

(defmethod hateoas-links (renderer (object test-enums) add-link)
  (declare (ignore renderer))
  (when (eql (slot-value object 'id) 1)
    (funcall add-link 'self "self_link")
    (funcall add-link 'extra "extra_link")))

(defmethod hateoas-links (renderer (object test-object) add-link)
  (declare (ignore renderer))
  (when (eql (slot-value object 'id) 1)
    (funcall add-link 'self "object_self_link")
    (funcall add-link 'extra (make-instance 'test-enums :id 1))))

(test enums
  (let ((renderer (make-instance 'json-renderer)))
    (is (string= "{\"id\":0,\"enum\":\"VALUE\",\"keywordEnum\":\"EXTRA\",\"integerEnum\":11}"
                 (render-json-string renderer (make-instance 'test-enums :id 0 :enum 'value :keyword-enum :extra :integer-enum 'value-1))))))

(test simple-object
  (let ((renderer (make-instance 'json-renderer)))
    (is (string= "{\"id\":0,\"description\":\"Simple Description\"}"
                 (render-json-string renderer (make-instance 'test-reference :id 0 :description "Simple Description"))))
    (is (string= "{\"id\":0,\"description\":null}"
                 (render-json-string renderer (make-instance 'test-reference :id 0 :description nil))))
    (is (string= "{\"id\":0}"
                 (render-json-string renderer (make-instance 'test-reference :id 0))))))

(test object-with-a-list
  (let ((renderer (make-instance 'json-renderer)))
    (is (string= "{\"id\":0,\"listAsObject\":{\"a\":\"A\",\"b\":\"B\"},\"listAsObjectKeyword\":{\"kA\":\"A\",\"kB\":\"B\"},\"listAsObjectString\":{\"s-a\":\"A\",\"s-b\":\"B\"}}"
                 (render-json-string renderer (make-instance 'test-object :id 0
                                                                          :list-as-object '((a . "A") (b . "B"))
                                                                          :list-as-object-keyword '((:k-a . "A") (:k-b . "B"))
                                                                          :list-as-object-string '(("s-a" . "A") ("s-b" . "B"))))))))

(test render-ignored-slots
  (let ((renderer (make-instance 'json-renderer)))
    (is (string= "{\"id\":0,\"parseIgnored\":\"ignored on parse\"}"
                 (render-json-string renderer (make-instance 'test-object
                                                             :id 0
                                                             :ignored "ignored"
                                                             :render-ignored "ignored on render"
                                                             :parse-ignored "ignored on parse"))))))

(test hash-map
  (let ((renderer (make-instance 'json-renderer))
        (map (make-hash-table)))
    (setf (gethash 'a map) (make-instance 'test-object
                                          :id 0
                                          :description "descr"
                                          :ignored "ignored"))
    (is (string= "{\"a\":{\"id\":0,\"description\":\"descr\"}}"
                 (render-json-string renderer map)))))

(test links
  (let ((renderer (make-instance 'json-renderer)))
    (is (string= "{\"_links\":{\"extra\":\"extra_link\",\"self\":\"self_link\"},\"id\":1}"
                 (render-json-string renderer (make-instance 'test-enums :id 1))))
    (is (string= "{\"_links\":{\"extra\":\"self_link\",\"self\":\"object_self_link\"},\"id\":1}"
                 (render-json-string renderer (make-instance 'test-object :id 1))))))

(test conversions
  (let ((renderer (make-instance 'json-renderer)))
    (is (string= "{\"id\":1,\"enumVector\":[11,12,11]}"
                 (render-json-string renderer (make-instance 'test-conversion
                                                             :id 1
                                                             :enum-vector #1A(value-1 value-2 value-1)))))
    (is (string= "{\"id\":0,\"keywordEnum\":[\"EXTRA\"]}"
                 (render-json-string renderer (make-instance 'test-enums
                                                             :id 0
                                                             :keyword-enum #(:extra)))))
    (is (string= "{\"id\":0,\"convertedString\":\"CONVERTED\"}"
                 (render-json-string renderer (make-instance 'test-conversion
                                                             :id 0
                                                             :converted-string "str"))))))
