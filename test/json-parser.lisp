(in-package :cl-json-object-tests)

(def-suite json-parser :in cl-json-object)

(in-suite json-parser)

(test slot-designator
  (let (cl-json-object::*object-init-params*
        cl-json-object::*object-slots*
        (designator (slot-value (find-class 'test-object) 'cl-json-object::slot-designator)))
    (funcall designator "id" 5)
    (funcall designator "subObject" "Reference")
    (funcall designator "uninitializedSlot" "Uninit")
    (is (= 4 (length cl-json-object::*object-init-params*)))
    (is (= 5 (getf cl-json-object::*object-init-params* :id)))
    (is (string= "Reference" (getf cl-json-object::*object-init-params* :reference)))
    (is (= 1 (length cl-json-object::*object-slots*)))
    (is (eq 'uninitialized-slot (car (first cl-json-object::*object-slots*))))
    (is (string= "Uninit" (cdr (first cl-json-object::*object-slots*))))
    ))

(test single-object-full
  (let ((object (parse-json-string 'test-object
                                   "{\"id\":55,\"uninitializedSlot\":\"Uninit\",\"description\":\"descr\"}")))
    (is (typep object 'test-object))
    (is (= 55 (slot-value object 'id)))
    (is (string= "descr" (slot-value object 'description)))
    (is (string= "Uninit" (slot-value object 'uninitialized-slot)))
    (is (not (slot-boundp object 'reference)))))

(test single-object-without-id
  (let ((object (parse-json-string 'test-object
                                   "{\"uninitializedSlot\":\"Assigned\",\"description\":\"Full Description\"}"
                                   :id 66)))
    (is (typep object 'test-object))
    (is (= 66 (slot-value object 'id)))
    (is (string= "Full Description" (slot-value object 'description)))
    (is (string= "Assigned" (slot-value object 'uninitialized-slot)))
    (is (not (slot-boundp object 'reference)))))

(test object-list
  (let ((objects (parse-json-string 'test-object
                                    "[{\"id\":55,\"uninitializedSlot\":\"Uninit\",\"description\":\"descr\"},
{\"id\":66,\"uninitializedSlot\":\"Init\",\"description\":\"Empty\"}]")))
    (is (= 2 (length objects)))
    (let ((object-1 (first objects))
          (object-2 (second objects)))
      (is (typep object-1 'test-object))
      (is (= 55 (slot-value object-1 'id)))
      (is (string= "descr" (slot-value object-1 'description)))
      (is (string= "Uninit" (slot-value object-1 'uninitialized-slot)))
      (is (not (slot-boundp object-1 'reference)))

      (is (typep object-2 'test-object))
      (is (= 66 (slot-value object-2 'id)))
      (is (string= "Empty" (slot-value object-2 'description)))
      (is (string= "Init" (slot-value object-2 'uninitialized-slot)))
      (is (not (slot-boundp object-2 'reference)))
      )))

(test single-object-partial-reference
  (let ((object (parse-json-string 'test-object
                                   "{\"uninitializedSlot\":\"Uninit\",\"description\":\"descr\",
\"subObject\":{\"id\":128}}"
                                   :id -5)))
    (is (typep object 'test-object))
    (is (= -5 (slot-value object 'id)))
    (is (string= "descr" (slot-value object 'description)))
    (is (string= "Uninit" (slot-value object 'uninitialized-slot)))
    (is (slot-boundp object 'reference))
    (let ((sub-object (slot-value object 'reference)))
      (is (typep sub-object 'test-reference))
      (is (= 128 (slot-value sub-object 'id))))))

(test single-object-full-reference
  (let ((object (parse-json-string 'test-object
                                   "{\"uninitializedSlot\":\"Uninit\",\"description\":\"descr\",
\"subObject\":{\"id\":128,\"description\":\"Subobject\"}}"
                                   :id -5)))
    (is (typep object 'test-object))
    (is (= -5 (slot-value object 'id)))
    (is (string= "descr" (slot-value object 'description)))
    (is (string= "Uninit" (slot-value object 'uninitialized-slot)))
    (is (slot-boundp object 'reference))
    (let ((sub-object (slot-value object 'reference)))
      (is (typep sub-object 'test-reference))
      (is (= 128 (slot-value sub-object 'id)))
      (is (string= "Subobject" (slot-value sub-object 'description))))))

(test single-object-array-of-reference
  (let ((object (parse-json-string 'test-object
                                   "{\"uninitializedSlot\":\"Uninit\",\"description\":\"descr\",
\"subObject\":[{\"id\":128},{\"id\":1024}]}"
                                   :id -5)))
    (is (typep object 'test-object))
    (is (= -5 (slot-value object 'id)))
    (is (string= "descr" (slot-value object 'description)))
    (is (string= "Uninit" (slot-value object 'uninitialized-slot)))
    (is (slot-boundp object 'reference))
    (let ((sub-objects (slot-value object 'reference)))
      (is (listp sub-objects))
      (is (= 2 (length sub-objects)))
      (let ((sub-object-1 (first sub-objects))
            (sub-object-2 (second sub-objects)))
        (is (typep sub-object-1 'test-reference))
        (is (= 128 (slot-value sub-object-1 'id)))
        (is (typep sub-object-2 'test-reference))
        (is (= 1024 (slot-value sub-object-2 'id)))))))

(test single-object-with-alist
  (let ((object (parse-json-string 'test-object
                                   "{\"id\":55,\"listAsObject\":{\"a\":\"A\",\"b\":\"B\"},\"listAsObjectKeyword\":{\"kA\":\"A\",\"kB\":\"B\"},\"listAsObjectString\":{\"s-a\":\"A\",\"s-b\":\"B\"}}")))
    (is (typep object 'test-object))
    (is (= 55 (slot-value object 'id)))
    (is (not (slot-boundp object 'reference)))
    (is (listp (slot-value object 'list-as-object)))
    (let ((alist (slot-value object 'list-as-object)))
      (is (string= "A" (cdr (assoc 'a alist))))
      (is (string= "B" (cdr (assoc 'b alist)))))
    (is (listp (slot-value object 'list-as-object-keyword)))
    (let ((alist (slot-value object 'list-as-object-keyword)))
      (is (string= "A" (cdr (assoc :k-a alist))))
      (is (string= "B" (cdr (assoc :k-b alist)))))
    (is (listp (slot-value object 'list-as-object-string)))
    (let ((alist (slot-value object 'list-as-object-string)))
      (is (string= "A" (cdr (assoc "s-a" alist :test #'string=))))
      (is (string= "B" (cdr (assoc "s-b" alist :test #'string=)))))))

(test single-object-with-alist-with-subobject
  (let ((object (parse-json-string 'test-object
                                   "{\"id\":55,\"listAsObject\":{\"a\":\"A\",\"b\":{\"c\":\"C\"}}}")))
    (is (typep object 'test-object))
    (is (= 55 (slot-value object 'id)))
    (is (not (slot-boundp object 'reference)))
    (is (listp (slot-value object 'list-as-object)))
    (let ((alist (slot-value object 'list-as-object)))
      (is (string= "A" (cdr (assoc 'a alist))))
      (is (listp (cdr (assoc 'b alist))))
      (is (string= "C" (cdr (assoc 'c (cdr (assoc 'b alist)))))))))

(test enum-values
  (let ((object (parse-json-string 'test-enums
                                   "{\"id\":55,\"enum\":\"VALUE\",\"packageEnum\":\"VALUE\",\"keywordEnum\":\"EXTRA\",\"integerEnum\":12}")))
    (is (typep object 'test-enums))
    (is (= 55 (slot-value object 'id)))
    (is (eq 'value (slot-value object 'enum)))
    (is (eq 'cl-json-object-tests.values::value (slot-value object 'package-enum)))
    (is (eq :extra (slot-value object 'keyword-enum)))
    (is (eq 'value-2 (slot-value object 'integer-enum)))))

(test string-integer-conversion
  (let ((object (parse-json-string 'test-conversion
                                   "{\"id\":55,\"integer\":\"55\"}")))
    (is (typep object 'test-conversion))
    (is (integerp (slot-value object 'integer)))
    (is (= 55 (slot-value object 'integer)))))

(test empty-string-integer-conversion
  (let ((object (parse-json-string 'test-conversion
                                   "{\"id\":55,\"integer\":\"\"}")))
    (is (typep object 'test-conversion))
    (is (null (slot-value object 'integer)))))

(test string-float-conversion
  (let ((object (parse-json-string 'test-conversion
                                   "{\"id\":55,\"float\":\"5.5\"}")))
    (is (typep object 'test-conversion))
    (is (floatp (slot-value object 'float)))
    (is (= 5.5 (slot-value object 'float)))))

(test integer-string-conversion
  (let ((object (parse-json-string 'test-conversion
                                   "{\"id\":55,\"string\":55}")))
    (is (typep object 'test-conversion))
    (is (stringp (slot-value object 'string)))
    (is (string= "55" (slot-value object 'string)))))

(test integer-float-conversion
  (let ((object (parse-json-string 'test-conversion
                                   "{\"id\":55,\"float\":5}")))
    (is (typep object 'test-conversion))
    (is (floatp (slot-value object 'float)))
    (is (= 5.0 (slot-value object 'float)))))

(test float-string-conversion
  (let ((object (parse-json-string 'test-conversion
                                   "{\"id\":55,\"string\":5.5}")))
    (is (typep object 'test-conversion))
    (is (stringp (slot-value object 'string)))
    (is (string= "5.5" (slot-value object 'string)))))

(test null-string-conversion
      (let ((object (parse-json-string 'test-conversion
                                       "{\"id\":55,\"string\":null}")))
        (is (typep object 'test-conversion))
        (is (null (slot-value object 'string)))))

(test string-timestamp-conversion
      (let ((object (parse-json-string 'test-conversion
                                       "{\"id\":55,\"timestamp\":\"2020-01-02T01:02:03Z\"}")))
        (is (typep object 'test-conversion))
        (is (local-time:timestamp= (local-time:parse-timestring "2020-01-02T01:02:03Z") (slot-value object 'timestamp)))))

(test null-timestamp-conversion
      (let ((object (parse-json-string 'test-conversion
                                       "{\"id\":55,\"timestamp\":null}")))
        (is (typep object 'test-conversion))
        (is (null (slot-value object 'timestamp)))))

(test list-vector-conversion
  (let ((object (parse-json-string 'test-conversion
                                   "{\"id\":55,\"vector\":[1, 2, 3]}")))
    (is (typep object 'test-conversion))
    (is (vectorp (slot-value object 'vector)))
    (is (equalp #1A(1 2 3) (slot-value object 'vector)))))

(test integer-list-string-vector-conversion
  (let ((object (parse-json-string 'test-conversion
                                   "{\"id\":55,\"stringVector\":[1, 2, 3]}")))
    (is (typep object 'test-conversion))
    (is (vectorp (slot-value object 'string-vector)))
    (is (equalp #1A("1" "2" "3") (slot-value object 'string-vector)))))

(test integer-list-enum-vector-conversion
  (let ((object (parse-json-string 'test-conversion
                                   "{\"id\":55,\"enumVector\":[11, 12, 11]}")))
    (is (typep object 'test-conversion))
    (is (vectorp (slot-value object 'enum-vector)))
    (is (equalp #1A(value-1 value-2 value-1) (slot-value object 'enum-vector)))))

(test ignore-links
  (signals undefined-property
    (parse-json-string 'test-enums
                       "{\"id\":55,\"enum\":\"VALUE\",\"packageEnum\":\"VALUE\",\"keywordEnum\":\"EXTRA\",\"_links\":{\"test\":\"test\"},\"_etag\":null}")))

(test ignore-custom-parameter
  (handler-bind ((undefined-property #'(lambda (e) (when (string= "_etag" (property-name e))
                                                     (invoke-restart 'ignore-property)))))
    (parse-json-string 'test-enums
                       "{\"id\":55,\"enum\":\"VALUE\",\"packageEnum\":\"VALUE\",\"keywordEnum\":\"EXTRA\",\"_links\":{\"test\":\"test\"},\"_etag\":null}"))
  (pass))

(test parse-ignored-slots
  (let ((object (parse-json-string 'test-object
                                   "{\"id\":55,\"ignored\":\"IGNORED\",\"renderIgnored\":\"PARSED\",\"parseIgnored\":\"PARSE-IGNORED\"}")))
    (is (typep object 'test-object))
    (is (= 55 (slot-value object 'id)))
    (is (string= "PARSED" (slot-value object 'render-ignored)))
    (is (not (slot-boundp object 'ignored)))
    (is (not (slot-boundp object 'parse-ignored)))))

(test scalar-converter
  (let ((object (parse-json-string 'test-conversion
                                   "{\"id\":55,\"convertedString\":\"str\"}")))
    (is (typep object 'test-conversion))
    (is (string= "RESTORED" (slot-value object 'converted-string)))))
