(defpackage :cl-json-object-tests
  (:use :cl
        :fiveam
        :cl-json-object))

(defpackage :cl-json-object-tests.values
  (:use :cl))

(in-package :cl-json-object-tests)

(def-suite cl-json-object)

(defmethod asdf:perform ((op asdf:test-op) (c (eql (asdf:find-system ':cl-json-object/tests))))
  (let ((results (run 'cl-json-object)))
    (unless (results-status results)
      (explain! results)
      (error "Some tests were failed"))))

