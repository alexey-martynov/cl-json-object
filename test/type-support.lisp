(in-package :cl-json-object-tests)

(def-suite utility :in cl-json-object)

(in-suite utility)

(test type-hierarchy
  (is (cl-json-object::type-included-p 'float '(or null float)))
  (is (cl-json-object::type-included-p 'float '(or null (float -10.0 10.0))))
  (is (cl-json-object::type-included-p 'integer '(or null float integer)))
  (is (not (cl-json-object::type-included-p 'float '(or null integer)))))

(test get-vector-type
  (is (eql 'vector (cl-json-object::vector-type 'vector)))
  (is (eql 'vector (cl-json-object::vector-type '(or null vector))))
  (is (equal '(vector integer) (cl-json-object::vector-type '(or null (vector integer))))))

(test get-array-element-type
  (is (eql '* (cl-json-object::array-type-element-type 'vector)))
  (is (eql '* (cl-json-object::array-type-element-type '(vector))))
  (is (eql '* (cl-json-object::array-type-element-type '(vector *))))
  (is (eql '* (cl-json-object::array-type-element-type '(vector * 15))))
  (is (eql 'integer (cl-json-object::array-type-element-type '(vector integer))))
  (is (eql 'integer (cl-json-object::array-type-element-type '(vector integer 15))))
  (is (equal '(integer 15 25) (cl-json-object::array-type-element-type '(vector (integer 15 25))))))

(test get-sequence-type
  (is (eql nil (cl-json-object::sequence-type 15)))
  (is (eql nil (cl-json-object::sequence-type "A")))
  (is (eql 'list (cl-json-object::sequence-type '(1 2 3))))
  (is (eql (first (type-of #(1 2 3))) (cl-json-object::sequence-type #(1 2 3)))))
