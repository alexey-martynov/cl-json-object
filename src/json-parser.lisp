(in-package :cl-json-object)

(define-condition undefined-property (error)
  ((name :initarg :name
         :reader property-name
         :documentation "JSON property name as read from document")
   (class :initarg :class
          :reader property-class
          :documentation "Class searched for JSON property"))
  (:report (lambda (condition stream)
             (format stream "Undefined JSON property \"~A\" in class ~A."
                     (property-name condition) (property-class condition)))))

(defun slot-type (class name)
  "Determine JSON-IGNORE and SLOT object for slot NAME in CLASS"
  ;;(format t "JSON-type for ~S~%" name)
  ;; Always ignore "_links"
  (cond
   ((string= "_links" name)
    (values t))
   ((eql 'a-list-object class)
    (values nil))
   (t
      (let* ((class-object (if (symbolp class) (find-class class) class))
             (slot (member name (closer-mop:class-slots class-object) :key #'json-property-name :test #'string=)))
        ;;(format t "Slot name ~S type ~S~%" name (when slot (first slot)))
        (if slot
            (values (json-ignore (first slot) :parse) (first slot))
            (restart-case
                (error 'undefined-property :class (class-name class-object) :name name)
              (ignore-property () (values t))))))))

(defun begin-object ()
  ;;(format t "Starting object ~S for ~S~%" *class-name* *value-name*)
  (case *class-name*
    ((nil)
     ;; Object is ignored, setup proper vars
     (setf *class-name* nil
           *object-init-params* '()
           *object-slots* '()
           *slot-designator* #'empty-json-slot-designator))
    (a-list-object
     ;; *class-name* and *slot-designator* already have correct values for AList handling
     (setf *object-init-params* '()
           *object-slots* '()))
    (otherwise
     ;;(format t "Begin oject ~S ~S~%" *class-name* *value-name*)
     (let ((parent-class (find-class *class-name*)))
       (unless *value-name*
         (setf *slot-designator* (slot-value parent-class 'slot-designator)))
       (when *value-name*
         (multiple-value-bind (ignore slot) (slot-type parent-class *value-name*)
           (if ignore
               ;; Internal slot just ignore everything
               (setf *class-name* nil
                     *object-init-params* '()
                     *object-slots* '()
                     *slot-designator* #'empty-json-slot-designator)
               (with-slots (json-type class) slot
                 ;;(format t "Slot ~S JSON type ~S class ~S~%" *value-name* json-type class)
                 (unless (or (eql :class json-type) (eql :a-list json-type))
                   (error "Slot ~S has incorrect type specification ~S, it should start with :CLASS" *value-name* json-type))
                 (let (class-object slot-designator)
                   (if (eql :a-list json-type)
                       (progn
                         ;;(format t "Handling AList~%")
                         (setf class 'a-list-object
                               slot-designator (if-let (package (slot-value slot 'package))
                                                 (lambda (name value) (alist-symbol-json-slot-designator package name value))
                                                 #'alist-json-slot-designator)))
                       (progn
                         (unless (find-class class)
                           (error "Slot ~S has incorrect class type specification ~S, doesn't contain a valid class" *value-name* class))
                         (setf class-object (find-class class))
                         ;;(format t "Found class ~S~%" class-object)
                         (closer-mop:ensure-finalized class-object)
                         (setf slot-designator (slot-value class-object 'slot-designator))))
                   (setf *class-name* class
                         *object-init-params* '()
                         *object-slots* '()
                         *slot-designator* slot-designator))))))))))

(defun end-object ()
  ;;(format t "Ending object ~S~%Initargs ~S~%Slots ~S~%" *class-name* *object-init-params* *object-slots*)
  (when *class-name*
    (if (eql 'a-list-object *class-name*)
        *object-slots*
        (let ((object (apply #'make-instance *class-name* *object-init-params*)))
          (mapc (named-lambda apply-slot (slot)
                  (setf (slot-value object (car slot)) (cdr slot)))
                *object-slots*)
          object))))

(defun object-key (name)
  ;;(format t "Property ~S in ~S~%" name *class-name*)
  (setf *value-name* name))

(defun convert-value (value json-type package converter value-type)
  ;;(format t "~%~S, Types ~S -> ~S, JSON ~S~%" value (type-of value)
  ;;        value-type json-type)
  (cond
    ;; Additional conversions to satisfy type system requirements
    ;; NIL always has special handling
    ((and (null value)
          (type-included-p 'null value-type))
     nil)
    ((and (not (eql t value-type))
          (type-included-p 'string value-type)
          (not (stringp value)))
     (with-output-to-string (s) (write value
                                       :base 10
                                       :radix nil
                                       :readably t
                                       :right-margin nil
                                       :stream s)))
    ((and (not (eql t value-type))
          (type-included-p 'integer value-type)
          (not (integerp value)))
     (etypecase value
       (string
        (if (and (type-included-p 'null value-type)
                 (string= "" value))
            nil
            (parse-integer value)))))
    ((and (not (eql t value-type))
          (type-included-p 'float value-type)
          (not (floatp value)))
     (etypecase value
       (integer
        (coerce value 'float))
       (string
        (with-input-from-string (in value) (read in)))))
    ((and (not (eql t value-type))
          (type-included-p 'vector value-type)
          (listp value))
     (let* ((vector (vector-type value-type))
            (element-type (array-type-element-type vector)))
       (coerce (if (and (eql '* element-type) (not json-type))
                   value
                   (mapcar (named-lambda !convert-element (item)
                             (convert-value item json-type package converter element-type))
                           value))
               vector)))
    ;; Scalar conversions when :TYPE is missing
    ((and (eql :scalar json-type) converter)
     (funcall converter :parse value))
    ((eql :enum json-type)
     (if converter
         (funcall converter :parse value)
         (progn
           (unless (stringp value)
             (error "Enumeration value should be represented as string"))
           (intern value package))))
    ((eql :timestamp json-type)
     (and value (local-time:parse-timestring value)))
    (t
     value)))

(defun object-value (value)
  (when *class-name*
    (multiple-value-bind (ignore slot) (slot-type *class-name* *value-name*)
      ;;(format t "Value of ~S (ignored ~S) for ~S is ~S~%" *value-name* ignore *class-name* value)
      (when (and (not ignore) slot)
        (with-slots (json-type package converter value-type) slot
          (setf value (convert-value value
                                     (if (slot-boundp slot 'json-type) json-type nil)
                                     (if (slot-boundp slot 'package) package nil)
                                     (if (slot-boundp slot 'converter) converter nil)
                                     (if (slot-boundp slot 'value-type) value-type t))))))
    ;;(format t "Value of ~S for ~S is ~S~%" *value-name* *class-name* value)
    (funcall *slot-designator* *value-name* value)
    ;;(format t "Designated parameters Initargs ~S~%Slots ~S~%" *object-init-params* *object-slots*)
    (setf *value-name* nil)))

(defun parse-json-string (type source &rest init-params)
  (closer-mop:ensure-finalized (find-class type))
  (let* ((cl-json:*object-scope-variables* '(*class-name* *value-name* *object-init-params* *object-slots* *slot-designator*))
         (*class-name* type)
         *value-name*
         (*object-init-params* init-params)
         (*object-slots* '())
         *slot-designator*)
    ;;(format t "Class name ~S~%" *class-name*)
    (let ((cl-json:*beginning-of-object-handler* #'begin-object)
          (cl-json:*end-of-object-handler* #'end-object)
          (cl-json:*object-key-handler* #'object-key)
          (cl-json:*object-value-handler* #'object-value))
      (cl-json:decode-json-from-string source))))
