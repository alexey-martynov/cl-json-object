
(in-package :cl-json-object)

(defclass json-renderer ()
  ((level :documentation "Maximum level of object detalization. 1 means only top object."
          :initarg :level
          :initform 1
          :accessor json-level)))

(defun render-reference (renderer object stream)
  (let ((uris '()))
    (hateoas-links renderer object (named-lambda !add-link (link uri)
                                     (cond
                                       ((string= (symbol-name link) "SELF")
                                        (unless (stringp uri)
                                          ;; FIXME: provide correct error
                                          (error "A link to 'self' should be a string"))
                                        (push (cons link uri) uris))
                                       ((stringp uri)
                                        (push (cons link uri) uris))
                                       (t
                                        (hateoas-links renderer uri (named-lambda !handle-self-link (object-link uri)
                                                                      (when (string= (symbol-name object-link) "SELF")
                                                                        (unless (stringp uri)
                                                                          ;; FIXME: provide correct error
                                                                          (error "A link to 'self' should be a string"))
                                                                        (push (cons link uri) uris))))))))
    (when uris
      (cl-json:as-object-member ("_links" stream)
        (cl-json:with-object (stream)
          (mapc (named-lambda add-references (item)
                  (cl-json:as-object-member ((funcall cl-json:*lisp-identifier-name-to-json* (symbol-name (car item))) stream)
                    (cl-json:encode-json (cdr item) stream)))
                uris)))
      t)))

(defun render-instance (renderer object stream level render-detail)
  (cl-json:with-object (stream)
    (let ((references-rendered (render-reference renderer object stream)))
      ;;(format t "~%** Rendering object ~S with level ~A and detail ~S~%" object level render-detail)
      (when (ccase render-detail
              (:complete
               t)
              (:by-level
               (or (< level (json-level renderer)) (not references-rendered)))
              (:links
               (not references-rendered)))
        ;;(format t "~%** Rendering fields for ~S~%" object)
        (mapc #'(lambda (item)
                  (let ((slot-name (closer-mop:slot-definition-name item)))
                    (when (and (not (json-ignore item :render)) (slot-boundp object slot-name))
                      (cl-json:as-object-member ((json-property-name item) stream)
                        (let ((value (slot-value object slot-name))
                              (json-type (slot-value item 'json-type)))
                          (cond
                            ((eql :scalar json-type)
                             (if value
                                 (let ((converter (slot-value item 'converter)))
                                   (render-json* renderer (if converter (funcall converter :render value) value) stream
                                                 :level (+ level 1)
                                                 :render-detail (slot-value item 'rendering)))
                                 (princ "null" stream)))
                            ((eql :bool json-type)
                             (princ (if value "true" "false") stream))
                            ((eql :timestamp json-type)
                             (if value
                                 (cl-json:encode-json (local-time:format-timestring nil value :timezone local-time:+utc-zone+)
                                                      stream)
                                 (princ "null" stream)))
                            ((eql :enum json-type)
                             (if value
                                 (let ((converter (slot-value item 'converter)))
                                   (if-let (type (sequence-type value))
                                     (cl-json:encode-json (map type
                                                               #'(lambda (item)
                                                                   (if converter
                                                                       (funcall converter :render item)
                                                                       (symbol-name item)))
                                                               value)
                                                          stream)
                                     (cl-json:encode-json (if converter
                                                              (funcall converter :render value)
                                                              (symbol-name value))
                                                          stream)))
                                 (princ "null" stream)))
                            ((eq :a-list json-type)
                             (cl-json:encode-json-alist value stream))
                            (t
                             (render-json* renderer value stream
                                           :level (+ level 1)
                                           :render-detail (if (eq :complete render-detail) :complete (slot-value item 'rendering))))))))))
              (closer-mop:class-slots (class-of object)))))))

(defun render-json* (renderer object stream &key (level 0) (render-detail :BY-LEVEL))
  ;; Refactor to a generic function?
  (cond
    ((listp object)
      (cl-json:with-array (stream)
        (mapc (named-lambda render-list-item (item)
                (cl-json:as-array-member (stream)
                    (render-json* renderer item stream
                                  :level level :render-detail render-detail)))
              object)))
    ((hash-table-p object)
     (cl-json:with-object (stream)
       (maphash (named-lambda render-hash-item (key item)
                  (cl-json:as-object-member (key stream)
                    (render-json* renderer item stream :level (1+ level) :render-detail render-detail)))
                object)))
    ((typep object 'standard-object)
     (render-instance renderer object stream level render-detail))
    (t
     (cl-json:encode-json object stream)))
  nil)

(defun render-json (renderer object stream)
  (render-json* renderer object stream
                :render-detail (if (eq :complete (json-level renderer)) :complete :by-level)))

(defun render-json-string (renderer object)
  (with-output-to-string (str)
    (render-json renderer object str)))
