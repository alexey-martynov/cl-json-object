(in-package :cl-json-object)

;; The following variables are used during the parsing
;; But some
(defparameter *class-name* nil
  "Symbol of the expected class name during read.")

(defparameter *object-init-params* nil
  "All parameters with their values which used in MAKE-INSTANCE call.
They're collected during reading object properties.")

(defparameter *object-slots* nil
  "Alist of all slots that should be assigned after creating instance.
They're collected during reading object properties.")

(defparameter *slot-designator* nil
  "A function of 2 parameters SLOT and VALUE which should update
*OBJECT-INIT-PARAMS* and *OBJECT-SLOTS* accordingly.")

(defparameter *value-name* nil
  "A name of the current handling property.")

(defmacro define-json-enum-converter (name &body mapping)
  "Define enumeration mapping function NAME. The body is in form
of individual mapping list. Individual mapping is a list of 2 values:
enumeration symbol and JSON value."
  `(defun ,name (direction value)
     (declare (type (member :render :parse)))
     (if (eq direction :render)
         (ccase value
           ,@mapping)
         (cond
           ,@(mapcar #'(lambda (item) `((equal ,(second item) value) ',(first item)))
                     mapping)
           (t (error "Invalid value ~S for enum" value))))))

(defun empty-json-slot-designator (name value)
  (declare (ignore name value)))

(defun alist-json-slot-designator (name value)
  (push (cons name value) *object-slots*))

(defun alist-symbol-json-slot-designator (package name value)
  (push (cons (intern (camel-case-to-lisp name) package) value) *object-slots*))

(defclass json-direct-slot-definition (closer-mop:standard-direct-slot-definition)
  ((name :initarg :json-property
         :initform nil
         :documentation "Name of the JSON property in case when it doesn't match to slot name")
   (json-type ;;:initarg :json-type
              ;;:initform nil
    :documentation "Value type for slot to be used during encoding/decoding. Allowed values are:
  '(:CLASS class)' (the CLASS stores symbol of class)
  ':ENUM' (in current package), '(:ENUM :PACKAGE package)' to specify exact package, '(:ENUM :CONVERTER converter)' to specify converter
  ':SCALAR' (NIL will be rendered as 'null'), '(:SCALAR :CONVERTER converter)' to use converter function,
  ':BOOL' (NIL will be rendered as 'false', all others as 'true')
  ':TIMESTAMP' to render LOCAL-TIME:TIMESTAMP as ISO8601 string and parse it back
  ':A-LIST' (keys as symbol in current package), '(:A-LIST :PACKAGE package) (keys as symbols in PACKAGE), '(:A-LIST :STRING)' (keys as strings),
      to render a-list as object and parse it back as a-list")
   (class :documentation "Class symbol when slot stores object")
   (package :documentation "Package when data type requires it,
for example, :ENUM")
   (converter :documentation "Converter function")
   (ignore :initarg :json-ignore
           :initform nil
           :documentation "Flag to hide or not slot in the JSON output. Valid values :PARSE (ignore during parse), :RENDER (ignore during render) and T (always ignore).")
   (rendering :initarg :json-render
              :initform :by-level
              :documentation "Object property rendering. Allowed values are:
':BY-LEVEL' (default, object is rendered according to tree depth, ':LINK' (object always rendered as keys + link)
and ':COMPLETE' (object always rendered as complete object).")
   (value-type :initarg :type
               :documentation "Designated value type according to Lisp
type system.")))

(defclass json-effective-slot-definition (closer-mop:standard-effective-slot-definition)
  ((name :initarg :json-property
         :initform nil
         :documentation "Name of the JSON property in case when it doesn't match to slot name")
   (json-type :initarg :json-type
              :initform nil
              :documentation "Value type for slot to be used during encoding/decoding. Allowed values are:
  '(:CLASS class)' (the CLASS stores symbol of class)
  ':ENUM' (in current package), '(:ENUM :PACKAGE package)' to specify exact package, '(:ENUM :CONVERTER converter)' to specify converter
  ':SCALAR' (NIL will be rendered as 'null'), '(:SCALAR :CONVERTER converter)' to use converter function,
  ':BOOL' (NIL will be rendered as 'false', all others as 'true')
  ':TIMESTAMP' to render LOCAL-TIME:TIMESTAMP as ISO8601 string and parse it back
  ':A-LIST' (keys as symbol in current package), '(:A-LIST :PACKAGE package) (keys as symbols in PACKAGE), '(:A-LIST :STRING)' (keys as strings),
      to render a-list as object and parse it back as a-list")
   (class :initarg :class
          :documentation "Class symbol when slot stores object")
   (package :initarg :package
            :documentation "Package when data type requires it,
for example, :ENUM")
   (converter :initarg :converter
              :initform nil
              :documentation "Converter function")
   (ignore :initarg :json-ignore
           :initform nil
           :documentation "Flag to hide or not slot in the JSON output. Valid values :PARSE (ignore during parse), :RENDER (ignore during render) and T (always ignore).")
   (rendering :initarg :json-render
              :initform :by-level
              :documentation "Object property rendering. Allowed values are:
':BY-LEVEL' (default, object is rendered according to tree depth, ':LINK' (object always rendered as keys + link)
and ':COMPLETE' (object always rendered as complete object).")
   (value-type :initarg :type
               :documentation "Designated value type according to Lisp
type system.")))

(defclass json-object-class (standard-class)
  ((keys :initform '()
         :reader json-object-keys)
   (slot-designator :initform #'empty-json-slot-designator)))

(defgeneric hateoas-links (renderer object add-link)
  (:documentation "Add HATEOAS links to OBJECT rendered with RENDERER.
The ADD-LINK is a function of 2 parameters (link symbol and URI designator)
which registers link. The link symbol is used by its name. The URI
designator can be a string with URI inside or object which provides a 'self' link."))

(defmethod hateoas-links (renderer object add-link)
  (declare (ignore object renderer add-link)))

(defgeneric json-property-name (slot)
  (:documentation "Get JSON name taken from slot option JSON-PROPERTY or constructed from slot name"))

(defgeneric json-property-type (slot)
  (:documentation "Get JSON type taken from slot option JSON-TYPE or NIL"))

(defgeneric json-property-class (slot)
  (:documentation "Get JSON type class taken from slot option CLASS when JSON-TYPE is :CLASS or NIL"))

(defgeneric json-ignore (slot mode)
  (:documentation "Detects whether slot should be ignored during JSON rendering (MODE :RENDER) or
                  parsing (MODE :PARSE) according to JSON-IGNORE slot option"))

(defmethod shared-initialize :after ((slot json-direct-slot-definition) slot-names
                                     &key json-type &allow-other-keys)
  (declare (ignore slot-names))
  (cond
    ((eq :keyword json-type)
     (with-slots (json-type package converter) slot
         (setf json-type :enum
               package (find-package 'keyword)
               converter nil)))
    ((eq :enum json-type)
     (with-slots (json-type package converter) slot
         (setf json-type :enum
               package *package*
               converter nil)))
    ((and (consp json-type) (eq :enum (first json-type)))
     (let ((package-def *package*)
           converter-def
           key)
       (dolist (item (rest json-type))
         (if key
             (progn
               (ccase key
                 (:package
                  (setf package-def (find-package item)))
                 (:converter
                  (setf converter-def item)))
               (setf key nil))
             (cond
               ((member item '(:package :converter))
                (setf key item))
               ((symbolp item)
                (warn "Deprecated JSON-TYPE enum ~S configuration is used, use (:ENUM :PACKAGE package) instead" json-type)
                (setf package-def (find-package item)))
               (t
                (error "Invalid JSON-TYPE enumeration option ~S" item)))))
       (with-slots (json-type package converter) slot
           (setf json-type :enum
                 package package-def
                 converter converter-def))))
    ((eq :a-list json-type)
     (with-slots (json-type package converter) slot
       (setf json-type :a-list
             package *package*
             converter nil)))
    ((and (consp json-type) (eq :a-list (first json-type)))
     (let (package-def
           key)
       (dolist (item (rest json-type))
         (if key
             (progn
               ;; It always :PACKAGE
               (setf package-def (find-package item))
               (setf key nil))
             (cond
               ((member item '(:package :string))
                (setf key item))
               (t
                (error "Invalid JSON-TYPE AList option ~S" item)))))
       (with-slots (json-type package converter) slot
         (setf json-type :a-list
               package package-def
               converter nil))))
    ((and (consp json-type) (eq :class (first json-type)))
     (with-slots ((json-type-slot json-type) class) slot
       (setf json-type-slot :class
             class (second json-type))))
    ((member json-type '(:scalar :bool :timestamp))
     (setf (slot-value slot 'json-type) json-type))
    ((and (consp json-type) (eq :scalar (first json-type)))
     (if (eq :converter (second json-type))
         (setf (slot-value slot 'json-type) :scalar
               (slot-value slot 'converter) (third json-type))))
    ((eql nil json-type)
     (setf (slot-value slot 'json-type) nil))
    (t
     ;; FIXME: provide correct error
     (error "Invalid value for JSON-TYPE ~S" json-type))))

(defmethod closer-mop:validate-superclass ((class json-object-class) (superclass standard-object))
  (declare (ignore class superclass))
  t)

(defmethod shared-initialize :after ((class json-object-class)
                                     slot-names
                                     &key keys &allow-other-keys)
  (declare (ignore slot-names))
  (setf (slot-value class 'keys) keys)
  ;;(describe class)
  )

(defmethod closer-mop:direct-slot-definition-class ((class json-object-class)
                                                    &key json-property json-type (json-ignore nil json-ignore-specified-p) json-render &allow-other-keys)
  (declare (ignore json-ignore))
  ;;(format t "Invoked for ~S ~S~%" json-property json-ignore)
  (if (or json-property json-type json-ignore-specified-p json-render)
      'json-direct-slot-definition
      (call-next-method)))

(defmethod closer-mop:effective-slot-definition-class ((class json-object-class) &rest initargs)
  (declare (ignore initargs))
  ;;(format t "Initargs ~S~%" initargs)
  ;; For simplicity at this time always use JSON-based definition
  'json-effective-slot-definition)

(defmethod closer-mop:compute-effective-slot-definition ((class json-object-class) name direct-slots)
  (declare (ignore name))
  (let* ((effective-slot (call-next-method))
         (definitions (remove-if #'(lambda (item)
                                     (not (typep item 'json-direct-slot-definition)))
                                 direct-slots))
         (direct-slot (first definitions)))
    (when direct-slot
      (setf (slot-value effective-slot 'name) (slot-value direct-slot 'name)
            (slot-value effective-slot 'json-type) (slot-value direct-slot 'json-type)
            (slot-value effective-slot 'ignore) (slot-value direct-slot 'ignore)
            (slot-value effective-slot 'rendering) (slot-value direct-slot 'rendering))
      (when (slot-boundp direct-slot 'class)
        (setf (slot-value effective-slot 'class) (slot-value direct-slot 'class)))
      (when (slot-boundp direct-slot 'package)
        (setf (slot-value effective-slot 'package) (slot-value direct-slot 'package)))
      (when (slot-boundp direct-slot 'converter)
        (setf (slot-value effective-slot 'converter) (slot-value direct-slot 'converter)))
      (when (slot-boundp direct-slot 'value-type)
        (setf (slot-value effective-slot 'value-type) (slot-value direct-slot 'value-type))))
    ;;(describe effective-slot)
    effective-slot))

(defmethod closer-mop:finalize-inheritance :after ((class json-object-class))
  ;; Build slot designator function for parser
  (let ((init-args '())
        (slots '()))
    (mapc (named-lambda handle-slot (slot)
            (when (and (typep slot 'json-effective-slot-definition) (not (json-ignore slot :parse)))
              (if-let (args (closer-mop:slot-definition-initargs slot))
                (push (cons (json-property-name slot) (first args)) init-args)
                (push (cons (json-property-name slot) (closer-mop:slot-definition-name slot)) slots))))
          (closer-mop:class-slots class))
    (setf (slot-value class 'slot-designator) (named-lambda slot-designator (name value)
                                                (if-let ((arg (assoc name init-args :test #'equal)))
                                                  (setf *object-init-params* (cons (cdr arg) (cons value *object-init-params*)))
                                                  (when-let ((slot (assoc name slots :test #'equal)))
                                                    (push (cons (cdr slot) value) *object-slots*)))))))

(defmethod json-property-name (slot)
  (let ((slot-name (closer-mop:slot-definition-name slot)))
    (funcall cl-json:*lisp-identifier-name-to-json* (symbol-name slot-name))))

(defmethod json-property-name ((slot json-effective-slot-definition))
  (if-let (name (slot-value slot 'name))
    name
    (call-next-method)))

(defmethod json-property-type (slot)
  nil)

(defmethod json-property-type ((slot json-effective-slot-definition))
  (slot-value slot 'json-type))

(defmethod json-property-class (slot)
  nil)

(defmethod json-property-class ((slot json-effective-slot-definition))
  (if (eq :class (slot-value slot 'json-type))
      (slot-value slot 'class)
      nil))

(defmethod json-ignore (slot mode)
  (declare (ignore slot mode))
  nil)

(defmethod json-ignore ((slot json-effective-slot-definition) mode)
  (let ((value (slot-value slot 'ignore)))
    (or (eql t value) (eql mode value))))
