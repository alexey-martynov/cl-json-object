(in-package :cl-json-object)

(defun type-included-p (type1 type2)
  "Return T when TYPE1 is included in TYPE2 by any its subtype."
  (cond
    ((and (consp type2)
          (eql 'or (first type2)))
     (reduce #'(lambda (result type)
                 (or result (subtypep type type1)))
             (rest type2)
             :initial-value nil))
    (t
     (subtypep type2 type1))))

(defun vector-type (type)
  "Get first VECTOR type from TYPE."
  (cond
    ((eql 'vector type)
     type)
    ((and (consp type)
          (eql 'vector (first type)))
     type)
    ((and (consp type)
          (eql 'or (first type)))
     (find-if #'(lambda (item)
                  (or (eql 'vector item)
                      (and (consp item)
                           (eql 'vector (first item)))))
              (rest type)))))

(defun array-type-element-type (type)
  "Get element type from array/vector TYPE. If element type is not
specialied returns '*. It is a caller responsibility to pass
array/vector typespec."
  (or (and (consp type) (second type)) '*))

(defun sequence-type (value)
  "Determin sequence type (VECTOR, LIST or something else) from VALUE.
If VALUE is not sequence returns NIL."
  (let ((type (type-of value)))
    (cond
      ((eql 'cons type)
       'list)
      ;; Special case: strings are arrays but should be treated as strings
      ((stringp value)
       nil)
      ((and (listp type) (subtypep type 'sequence))
       (first type))
      (t
       nil))))
